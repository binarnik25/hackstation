> Elliot is a young programmer in New York City who works as a cyber-security engineer by day and as a vigilante hacker by night. He's a loner and occasional drug user with a social phobia of being around most people. He believes in using his computing talents to help those he cares about, as well as to punish those he believes are bringing harm to others.

*Pre: Help Elliot to setup programming environment. There is hack **station**, made by yourself through the web console in your browser. Zero eps is presenting the basics of smart contracts interaction. For this purpose we have to check your readiness. Only for this contest we forked Ethernaut console (and partially this installation zero step). So you can use it for interaction, thanks [martriay](https://github.com/martriay)*

&nbsp;
#### 1. Set up MetaMask
If you don't have it already, install the MetaMask browser extension (in either Chrome or Firefox desktop).
Set up the extension's wallet and use the network selector to point to the 'Ropsten test network' in the top left of the extension's interface.

&nbsp;
#### 2. Open the browser's console
Open your browser's console: `Tools > Developer Tools`.

You should see a few messages from the station. One of them should state your player's address. This will be important during the game! You can always see your player address by entering the following command:
```
player
```

Keep an eye out for warnings and errors, since they could provide important information during gameplay.

&nbsp;
#### 3. Use the console helpers

You can also see your current ether balance by typing:
```
getBalance(player)
```
###### NOTE: Expand the promise to see the actual value, even if it reads "pending". If you're using Chrome v62, you can use `await getBalance(player)` for a cleaner console experience.

Great! To see what other utility functions you have in the console type:
```
help()
```
These will be super handy during gameplay.


&nbsp;
#### 4. Get test ether
To play the game, you will need test ether. The easiest way to get some testnet ether is via [the MetaMask faucet](https://faucet.metamask.io/).

Once you see some ether in your balance, move on to the next step.

&nbsp;
#### 5. Getting a eps instance
When playing a eps, you don't interact directly with the station contract. Instead, you ask it to generate a **eps instance** for you. To do so, click the blue button at the bottom of the page. Go do it now and come back!

You should be prompted by MetaMask to authorize the transaction. Do so, and you should see some messages in the console. Note that this is deploying a new contract in the blockchain and might take a few seconds, so please be patient when requesting new eps instances!

&nbsp;
#### 6. Inspecting the contract
Just as you did with the station contract, you can inspect this contract's ABI through the console using the `contract` variable.

&nbsp;
#### 7. Interact with the contract to complete the eps
Look into the eps's run method
```
contract.run()
```
###### `await contract.run()` if you're using Chrome v62.
You should have all you need to complete the eps within the contract.
When you know you have completed the eps, submit the contract using the orange button at the bottom of the page.
This sends your instance back to the station, which will determine if you have completed it.


##### Tip: don't forget that you can always look in the contract's ABI!
