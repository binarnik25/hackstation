>This eps tells the story about one of the main power of confrontation. The Dark Army is a notorious "hacker-for-hire" collective based out of the People's Republic of China, led by the mysterious Whiterose. With alleged ties to Iran, Russia, and North Korea, the Dark Army has a reputation for ruthless efficiency.
>
>The Dark Army goes to great lengths to protect its security. The organization is highly compartmentalized, with need-to-know communication taking place through cutouts or IRC; face-to-face meetings with Dark Army leaders are rare.. 


*Pre: Someone can find the Dark Army’s wallet in Ethereum network. This one uses for different purposes of Dark Army before the mass attack. You can steal all of assets and prevent a crime.*

-------
Task: Reset wallet balance to 0
