> After being shoved off the railing in Coney Island by Mr. Robot, Elliot wakes up in a hospital room, badly banged up. Krista, his psychiatrist, is inclined to send him to rehab unless he’s willing to be regularly drug tested. Elliot complies, knowing that he can easily change the hospital records for the tests since he hacked their system long ago.

*Pre: Hospital records stores the results of regular drug testing. This is mapping of data by dates. Contract has methods for adding new data, granted only to the some person.*

--------
Task: Add value `true` with data `03/15/2018`, like in a previous days
