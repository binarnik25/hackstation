>At his day job at a cyber-security firm Allsafe, Elliot with colleagues attempting to stop a DDoS attack on E Corp's servers. E Corp (which Elliot refers to as "Evil Corp”) is their largest client, the multi-national conglomerate.
>
>Elliot realizes that they cannot stop the hack locally because of the rootkit that the hackers wrote and placed in the root directory of the server (CS 30), and together with Allsafe's owner he flies to E Corp's server farm to stop the hack in person.

*Pre: While examining the hacked server, Elliot finds a file with a message in it for him. The message simply says "Leave me here", and after a quick debate with himself he changes the file so that only he can access it but leaves it on the server.*

-------
Task: Contract has some public methods. Explore the web3.js library and make interaction with smart contract object. Change access to appropriate file and prevent the DDoS attack

*NOTE! the name of root directory could be important...*
