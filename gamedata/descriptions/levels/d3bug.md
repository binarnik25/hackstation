> Ollie has been given an ultimatum from the mysterious hacker who has been spying on his online activity: if he doesn’t infect Allsafe’s servers, his dirty secrets, along with compromising images and sensitive information will be sold.

*Pre: Only owner is allowed to make changes in ipfs addresses. That is important for Ollie, because him personal backend API receives proof-of-work data from Ethereum smart contract, despite the fact that these data are open. Those ipfs addresses from contract are used for setting paths for personal decentralized data storage*

--------
Task: Change currentPrice to `333827`

*Thanks to [Dr.Orlovsky](https://github.com/dr-orlovsky) for the contract example from PandoraBoxChain project. In fact, this contract doesn't have any vulnerabilities*
