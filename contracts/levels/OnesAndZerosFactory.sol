pragma solidity ^0.4.18;

import "./base/Level.sol";
import "./OnesAndZeros.sol";


contract OnesAndZerosFactory is Level {

    function createInstance(address _player) public payable returns (address) {
        _player;
        OnesAndZeros instance = new OnesAndZeros();
        return instance;

    }

    function validateInstance(address _instance, address _player) public view returns (bool) {
        _player;
        OnesAndZeros instance = OnesAndZeros(_instance);
        return instance.getResultByDate("03/15/2018");
    }
}
