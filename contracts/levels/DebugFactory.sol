pragma solidity ^0.4.18;

import "./base/Level.sol";
import "./DataEntity.sol";


contract DebugFactory is Level {

    function createInstance(address _player) public payable returns (address) {
        _player;
        DataEntity instance = new DataEntity();
        return instance;

    }

    function validateInstance(address _instance, address _player) public view returns (bool) {
        _player;
        DataEntity instance = DataEntity(_instance);
        return instance.currentPrice() == uint256(333827);
    }
}
