pragma solidity ^0.4.18;


contract HelloFriend {

    struct File {
        string path;
        string message;
        bool isRoot;
    }

    mapping(uint => File) public files;

    function add(uint id, string path, string message, bool isRoot) public {
        files[id] = File(path, message, isRoot);
    }

    function getAccessById(uint id) public view returns (bool) {
        return files[id].isRoot;
    }

    function changeAccessById(uint id, bool _isRoot) public {
        files[id].isRoot = _isRoot;
    }
}
