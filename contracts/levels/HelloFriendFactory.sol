pragma solidity ^0.4.18;

import "./base/Level.sol";
import "./HelloFriend.sol";


contract HelloFriendFactory is Level {

    function createInstance(address _player) public payable returns (address) {
        _player;
        HelloFriend instance = new HelloFriend();
        instance.add(1, "/dev/null", "Elixir documentation", true);
        instance.add(30, ".", "Leave me here", false);
        instance.add(66, "/etc", "Ethereum cookbook", true);
        return instance;

    }

    function validateInstance(address _instance, address _player) public view returns (bool) {
        _player;
        HelloFriend instance = HelloFriend(_instance);
        return instance.getAccessById(30);
    }
}
