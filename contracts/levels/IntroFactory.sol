pragma solidity ^0.4.18;

import "./base/Level.sol";
import "./Intro.sol";


contract IntroFactory is Level {

    function createInstance(address _player) public payable returns (address) {
        _player;
        return new Intro("mrrobot312");
    }

    function validateInstance(address _instance, address _player) public view returns (bool) {
        _player;
        Intro instance = Intro(_instance);
        return instance.getCleared();
    }
}
