pragma solidity ^0.4.18;


contract Intro {

    string public suspicious;
    bool private cleared = false;

    // constructor
    function Intro(string _password) public {
        suspicious = _password;
    }

    function run() public pure returns (string) {
        return "Contract has some methods. Go to info() method";
    }

    function info() public pure returns (string) {
        return "There is a station contract. For more details read the message()";
    }

    function message() public pure returns (string) {
        return "Who is the hero of this game? Use checkTheName() method with appropriate param";
    }

    function checkTheName(string param) public pure returns (string) {
        if (keccak256(param) == keccak256("Elliot") || keccak256(param) == keccak256("elliot")) {
            return "Good. Now, try to getAccess() for advanced station settings. If you know the param for this method..Carefully examine the contract";
        }
        return "Wrong parameter.";
    }

    function getAccess(string passkey) public {
        if (keccak256(passkey) == keccak256(suspicious)) {
            cleared = true;
        }
    }

    function getCleared() public view returns (bool) {
        return cleared;
    }
}
