
Invitation message
This story has roots from Mr. Robot Tv-series. Also, Ethernaut game has strong impact on this contest tasks. The main idea of Mr. Robot game is inspire to explore Ethereum architecture and apply brand new approach in software development. There is EVM, dude. Thinking decentralization.

Pre-requisites:
install MetaMask plugin for Chrome or Firefox
get some test ethers (Ropsten)
read docs of javascript library `web3.js` (for smart contracts interaction)


Difficulty:
Angela
Darlene
Mr. Robot
AllSafe
Dark Army
E Corp


Eps:
0x0_intro Station usage
0x1_hellofriend Prevent DDoS attack - rename rootkit
0x2_ones-and-zer0es Hack the medicine card - change test results..
0x3_d3bug Prevent infection of AllSafe’s servers - change owner with missing constructor
0x4_da3m0ns Hack climat control system - change state with integer overflow attack
0x5_3xpl0its Rob the Dark Army wallet - reentrancy attack





0x0 intro

“But I'm only a vigilante hacker by night. By day, just a regular cybersecurity engineer. Employee number ER28-0652”
												— Elliot

Elliot is a young programmer in New York City who works as a cyber-security engineer by day and as a vigilante hacker by night. He's a loner and occasional drug user with a social phobia of being around most people. He believes in using his computing talents to help those he cares about, as well as to punish those he believes are bringing harm to others.

Pre: Help Elliot to setup programming environment. There is hack station, made by yourself through the web console in your browser. Zero eps is presenting the basics of smart contracts interaction. For this purpose we have to check your readiness. Only for this contest we forked Ethernaut console (and partially the installation zero step). So you can use it for interaction, thanks martriay.



Task: Set up MetaMask
If you don't have it already, install the MetaMask browser extension (in either Chrome or Firefox desktop). Set up the extension's wallet and use the network selector to point to the 'Ropsten test network' in the top left of the extension's interface



Task: Open the browser's console
Open your browser's console: Tools > Developer Tools
You should see a few messages from the station. One of them should state your player's address. This will be important durin g the process! You can always see your player address by entering the following command: player
Keep an eye out for warnings and errors, since they could provide important information during gameplay.



Task: Use the console helpers
You can also see your current ether balance by typing: getBalance(player)
NOTE: Expand the promise to see the actual value, even if it reads "pending". If you're using Chrome v62, you can use await getBalance(player) for a cleaner console experience.

Great! To see what other utility functions you have in the console type: help()
These will be super handy during gameplay.



Task: The station contract
Enter the following command in the console: station
This is the contest's main smart contract. You don't need to interact with it directly through the console (as this app will do that for you) but you can if you want to. Playing around with this object now is a great way to learn how to interact with the contest's other smart contracts.

Go ahead and expand the station object to see what's inside.


Task: Get test ether
To play the game, you will need test ether. The easiest way to get some testnet ether is via the MetaMask faucet.
Once you see some ether in your balance, move on to the next step.


Task: Getting a level instance
When playing a level, you don't interact directly with the station contract. Instead, you ask it to generate a level instance for you. To do so, click the blue button at the bottom of the page. Go do it now and come back!

You should be prompted by MetaMask to authorize the transaction. Do so, and you should see some messages in the console. Note that this is deploying a new contract in the blockchain and might take a few seconds, so please be patient when requesting new level instances!

Task: Interact with the contract to complete the level
Look into the levels's info method: station.run()
await station.run() if you're using Chrome v62.


You should have all you need to complete the level within the contract. When you know you have completed the level, submit the contract using the orange button at the bottom of the page. This sends your instance back to the station, which will determine if you have completed it.

Tips: Don't forget that you can always look in the contract's ABI!


0x1 hellofriend
At his day job at a cyber-security firm Allsafe, Elliot with colleagues attempting to stop a DDoS attack on E Corp's servers. E Corp (which Elliot refers to as "Evil Corp”) is their largest client, the multi-national conglomerate.

Elliot realizes that they cannot stop the hack locally because of the rootkit that the hackers wrote and placed in the root directory of the server (CS 30), and together with Allsafe's owner he flies to E Corp's server farm to stop the hack in person.

Pre: While examining the hacked server, Elliot finds a file with a message in it for him. The message simply says "Leave me here", and after a quick debate with himself he changes the file so that only he can access it but leaves it on the server.

Task: Contract has some public methods. Explore the web3.js library and make interaction with smart contract object. Change access to appropriate file and prevent the DDoS attack

Tips: Use rename method to set right access for CS30 dir


0x2 ones-and-zer0es
After being shoved off the railing in Coney Island by Mr. Robot, Elliot wakes up in a hospital room, badly banged up. Krista, his psychiatrist, is inclined to send him to rehab unless he’s willing to be regularly drug tested. Elliot complies, knowing that he can easily change the hospital records for the tests since he hacked their system long ago.

Pre: Hospital records stores the results of regular drug testing. This is mapping of data by dates. Contract has methods for adding new data, granted only to the doctor.

Task: Change last value from true to false by the appropriate method

Tips: Add address to doctor’s mapping and use the method

0x3 d3bug
Ollie has been given an ultimatum from the mysterious hacker who has been spying on his online activity: if he doesn’t infect Allsafe’s servers, his dirty secrets, along with compromising images and sensitive information will be sold.

Pre: Only owner is allowed to make changes in ipfs addresses. That is important for Ollie, because him personal backend API receives proof-of-work data from Ethereum smart contract, despite the fact that these data are open. Those ipfs addresses from contract are used for setting paths for personal decentralized data storage

Task: Change the contract owner

Tips: Typo in class constructor


0x4 da3m0ns
At fsociety, Elliot lays out his plan to hack Steel Mountain’s climate control system and raise the heat high enough to melt all the tapes that record the data.

Pre: That climate system grab temperature from smart contract, which means as an oracle contract. There is simulation of real world. In fact, oracle’s system more complicated then one smart contract. Right now, the contract has storage for the value of the temperature, which is limited by natural conditions. If the temperature become an extremely large, then it could be destroy the data.

Task: Change the temperature to extremely large value

Tips: Intereg overflow



0x5 3xpl0its
This eps tells the story about one of the main power of confrontation. The Dark Army is a notorious "hacker-for-hire" collective based out of the People's Republic of China, led by the mysterious Whiterose. With alleged ties to Iran, Russia, and North Korea, the Dark Army has a reputation for ruthless efficiency.

The Dark Army goes to great lengths to protect its security. The organization is highly compartmentalized, with need-to-know communication taking place through cutouts or IRC; face-to-face meetings with Dark Army leaders are rare.. 

Pre: Someone can find the Dark Army’s wallet. This one uses for different purposes of Dark Army before the mass attack. You can steal all of assets and prevent a crime.

Task: Reset a balance to 0

Tips: Reentrancy attack
