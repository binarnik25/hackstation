import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as constants from '../constants'

class Home extends React.Component {

  navigateToFirstIncompleteLevel() {

    // Find first incomplete level
    let target = this.props.levels[0].deployedAddress
    for(let i = 0; i < this.props.levels.length; i++) {
      const level = this.props.levels[i]
      const completed = this.props.completedLevels[level.deployedAddress]
      if(!completed) {
        target = level.deployedAddress
        break
      }
    }

    // Navigate to first incomplete level
    this.props.router.push(`${constants.PATH_LEVEL_ROOT}${target}`)
  }

  render() {
    return (
      <div
        className="row"
        style={{
        paddingLeft: '40px',
        paddingRight: '40px',
      }}>

        <div className="col-sm-8">
          {/* TITLE */}
          <h2 className="title">
            Mr. Robot
          </h2>
          {/* INFO */}
          <p>There is a Web3/Solidity based application with gaming activity, played in the Ethereum Virtual Machine.</p>
          <p>This story has roots from <a href="https://www.imdb.com/title/tt4158110/" target="_blank" rel="noopener noreferred">Mr. Robot</a> Tv-series.</p>
          <p>Also, <a href="https://github.com/OpenZeppelin/ethernaut" target="_blank" rel="noopener noreferred">Ethernaut game </a> has strong impact on this contest tasks. The main idea of Mr. Robot game is inspire to explore Ethereum architecture and apply brand new approach in software development. There is EVM, dude.</p>
          <p>Thinking decentralization</p>
          <br/>
          <p>This contest tasks not so serious and just for make fun on the sctroopers conference.</p>
          <button
            style={{marginTop: '10px'}}
            className="btn btn-primary"
            onClick={() => this.navigateToFirstIncompleteLevel()}
          >
            go go go
          </button>
        </div>

        <div className="col-sm-4">
          <img style={{maxWidth: '100%', padding: '40px 0 20px 0'}} src='../../imgs/fsociety1.png' alt='fsociety'/>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    levels: state.gamedata.levels,
    completedLevels: state.player.completedLevels
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
